#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#define BB while(getchar()!='\n')
#define ERROR_OBRIR_FITXER "Error en obrir fitxer"
#define ERROR_ESCRIPTURA "Error de escriptura al fitxer"
#define ERROR_LECTURA_FITXER "Error de lectura"
#define ERROR_RENOMBRAR_FITXER "No ha pogut renombrar el fitxer"
#define ERROR_NO_POT_ESBORRAR_FITXER "Error no pot esborrar fitxer"
#define INFORMACIO_OPERACIO_CANCELADA "Operacio cancel·lada"
#define INFORMACIO_NO_ERRORS "Operacio realitzada amb exit"
#define MAX_NOM 20
#define MAX_TIPUS 20
#define NOM_FITXER "animals.dat"

typedef struct{
    char nom[MAX_NOM+1];
    char tipus[MAX_TIPUS+1];
    char sexe; //M-Masculi , F-Femeni
    int edat;
    double pes;
    bool enPerillExtincio;
    char marcaEsborrat;
}Animal;

void pintarMenu();
int alta(char nomFixer[]);
void entrarAnimal(Animal *animal, bool modifica);
void escriureAnimal(Animal animal);
int consulta(char nomFitxer[], bool esborrats);
int baixaModificacio(char nomFitxer[], bool modifica);
bool seguirBaixaModificacio(bool actualitza);
int esborrarFixerMenu(char nomFitxer[]);
int esborrarFitxer(char nomFitxer[]);
int compactarFitxer(char nomFitxer[]);

int main()
{
    int opcio, error;

    do{
        pintarMenu();
        scanf("%d",&opcio);BB;
        error=0;
        switch(opcio){
            case 1:
                error=alta(NOM_FITXER);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-2) printf(ERROR_ESCRIPTURA);
                break;
            case 2:
                error=baixaModificacio(NOM_FITXER,false);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-2) printf(ERROR_ESCRIPTURA);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                if(error==-4) printf("\n"INFORMACIO_OPERACIO_CANCELADA);
                break;
            case 3:
                error=consulta(NOM_FITXER,false);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                break;
            case 4:
                error=baixaModificacio(NOM_FITXER,true);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-2) printf(ERROR_ESCRIPTURA);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                if(error==-4) printf("\n"INFORMACIO_OPERACIO_CANCELADA);
                break;
            case 5:
                error=consulta(NOM_FITXER,true);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                break;
            case 6:
                error=esborrarFixerMenu(NOM_FITXER);
                if(error==-4) printf("\n"INFORMACIO_OPERACIO_CANCELADA);
                if(error==-6) printf(ERROR_NO_POT_ESBORRAR_FITXER);
                break;
            case 7:
                error=compactarFitxer(NOM_FITXER);
                if(error==-1) printf(ERROR_OBRIR_FITXER);
                if(error==-3) printf(ERROR_LECTURA_FITXER);
                if(error==-5) printf(ERROR_RENOMBRAR_FITXER);
                if(error==1) printf(INFORMACIO_NO_ERRORS);
                break;
            case 0:
                printf("Opcio 0\n");
                break;
            default:
                printf("Opcio incorrecta. Les opcions son de 0 a 7\n");
        }
        if(error!=0){
            printf("\nPrem una tecla per continuar...");
            getchar();
        }
    }while(opcio!=0);
    return 0;
}

void pintarMenu(){
    fflush(stdout);
    system("clear");
    printf("\n\t*** Menu ***\n\n");
    printf("\t1- Alta\n");
    printf("\t2- Baixa\n");
    printf("\t3- Consulta\n");
    printf("\t4- Modificacions\n");
    printf("\t5- Consulta esborrats\n");
    printf("\t6- Esborrar fitxer\n");
    printf("\t7- Compactar fitxer\n\n");
    printf("\t0- Sortir\n\n");
    printf("Tria Opcio(0-7): ");
}

int alta(char nomFixer[]){
    int n;
    Animal a1;
    FILE *f1;
    f1=fopen(nomFixer,"ab");
    if(f1==NULL) return -1;
    entrarAnimal(&a1,false);
    n=fwrite(&a1,sizeof(Animal),1,f1);
    if(n==0) return -2;
    fclose(f1);
    return 0;
}

void entrarAnimal(Animal *animal, bool modifica){
    char enPerill;
    if(!modifica){
        printf("\nIntrodueix el nom: ");
        scanf("%20[^\n]",animal->nom);BB;
    }
    printf("\nIntrodueix el tipus: ");
    scanf("%20[^\n]",animal->tipus);BB;
    do{
        printf("\nIntrodueix el sexe (m/f): ");
        scanf("%c",&animal->sexe);BB;
    }while(animal->sexe!='m' && animal->sexe!='f');
    printf("\nIntrodueix la edat: ");
    scanf("%d",&animal->edat);BB;
    printf("\nIntrodueix el pes: ");
    scanf("%lf",&animal->pes);BB;
    do{
        printf("Esta en perill de extincio (s/n)? ");
        scanf("%c",&enPerill);BB;
    }while(enPerill!='s' && enPerill!='n');
    if(enPerill=='s')animal->enPerillExtincio=true;
    else animal->enPerillExtincio=false;
}

void escriureAnimal(Animal animal){
    printf("\nNom: %s",animal.nom);
    printf("\nTipus: %s",animal.tipus);
    printf("\nSexe: %c",animal.sexe);
    printf("\nEdat: %d",animal.edat);
    printf("\nPes: %.2lf",animal.pes);
    if(animal.enPerillExtincio==true)printf("\nEsta en perill de extincio\n");
    else printf("\nNo esta en perill de extincio\n");
}

int consulta(char nomFitxer[], bool esborrats){
    int n;
    Animal a1;
    FILE *f1;
    f1=fopen(nomFitxer,"rb");
    if(f1==NULL)return -1;
    system("clear");
    while(!feof(f1)){
        n=fread(&a1,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n==0) return -3;
                if(!esborrats){
                    if(a1.marcaEsborrat!='*') escriureAnimal(a1);
                }else{
                    if(a1.marcaEsborrat=='*') escriureAnimal(a1);
                }
        }
    }
    fclose(f1);
    printf("\nPrem una tecla per continuar...");getchar();
    return 0;
}

bool seguirBaixaModificacio(bool actualitza){
    char opcio;
    bool segur=false;

    do{
        if(actualitza){
            printf("\nSegur que vols fer la actualitzacio (s/n)? ");
        }else{
            printf("\nSegur que vols esborrar-lo (s/n)? ");
        }
        scanf("%c",&opcio);BB;
    }while(opcio!='s' && opcio!='n');
    if(opcio=='s')segur=true;
    return segur;
}

//parametre: modifica es tipus bolea, si es fals fa la baixa, si es cert fa la modificacio
int baixaModificacio(char nomFitxer[], bool modifica){
    system("cls || clear");
    Animal a1;
    FILE *f1;
    char nomTmp[MAX_NOM];
    int n = 0;
    f1 = fopen(nomFitxer,"rb+");
    if(f1 == NULL){
        return -1;
    }
    printf("\nIntrodueix el nom: ");
    scanf("%15[^\n]",nomTmp);BB;
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animal),1,f1);
        if(!feof(f1)){
            if(n == 0){
                return -3;
            }
            if(a1.marcaEsborrat != '*' && strcmp(a1.nom,nomTmp) == 0){//Si no ¿Esta borrat? I trobat
                escriureAnimal(a1);
                if(!seguirBaixaModificacio(modifica))return -4;
                if(fseek(f1, -(long) sizeof(Animal), SEEK_CUR)) return -2;//-1 posicion
                if(modifica){
                    entrarAnimal(&a1,modifica);
                }else{
                    a1.marcaEsborrat = '*';
                }
                n = fwrite(&a1, sizeof(Animal), 1, f1);//Escriure en el disc
                if(n == 0) return -3;//Control error
                break;
            }
        }
    }
    fclose(f1);
    return 0;
}

int esborrarFixerMenu(char nomFitxer[]){
    char sn;
    int n=0;
    do{
        printf("Segur que vols esborrar el fitxer (s/n)? ");
        scanf("%c",&sn);BB;
    }while(sn!='s' && sn!='n');
    if(sn=='s'){
        n=esborrarFitxer(nomFitxer);
        if(n!=0){
            n=-6;
        }
    }else n=-4;
    return n;
}

int esborrarFitxer(char nomFitxer[]){
    return unlink(nomFitxer);
}

int compactarFitxer(char nomFitxer[]){
    Animal animal;
    FILE *origen, *desti;
    int n;
    origen=fopen(nomFitxer,"rb");
    if(origen==NULL)return -1;
    desti=fopen("tmp.dat","wb");
    if(desti==NULL)return -1;

    while(!feof(origen)){
        n=fread(&animal,sizeof(Animal),1,origen);
        if(!feof(origen)){
            if(n==0) return -3;
            if(animal.marcaEsborrat!='*'){
            n=fwrite(&animal,sizeof(Animal),1,desti);
            }
        }
    }
    fclose(origen);
    fclose(desti);
    esborrarFitxer(nomFitxer);
    if(rename("tmp.dat",NOM_FITXER)==-1){
        return -5;
    }
    return 1;
}
